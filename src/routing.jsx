import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import App from "./App.js";

const routing = (
  <Router>
    <Switch>
      <Route path="/" component={App} />
      <Route path="/active" render={props => <App {...props} />} />
      <Route path="/completed" component={App} />
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));
